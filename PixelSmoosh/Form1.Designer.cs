﻿namespace PixelSmoosh
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_setOutput = new System.Windows.Forms.Button();
            this.btn_setSourceDir = new System.Windows.Forms.Button();
            this.btn_setTarget = new System.Windows.Forms.Button();
            this.btn_generateImage = new System.Windows.Forms.Button();
            this.lbl_disp_targetPath = new System.Windows.Forms.Label();
            this.lbl_disp_outputPath = new System.Windows.Forms.Label();
            this.lbl_disp_status = new System.Windows.Forms.Label();
            this.dlg_selectFolder = new System.Windows.Forms.FolderBrowserDialog();
            this.dlg_selectOpen = new System.Windows.Forms.OpenFileDialog();
            this.dlg_selectSave = new System.Windows.Forms.SaveFileDialog();
            this.lbl_disp_sourcePath = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_setOutput
            // 
            this.btn_setOutput.Location = new System.Drawing.Point(188, 182);
            this.btn_setOutput.Name = "btn_setOutput";
            this.btn_setOutput.Size = new System.Drawing.Size(75, 23);
            this.btn_setOutput.TabIndex = 0;
            this.btn_setOutput.Text = "Output";
            this.btn_setOutput.UseVisualStyleBackColor = true;
            this.btn_setOutput.Click += new System.EventHandler(this.btn_setOutput_Click);
            // 
            // btn_setSourceDir
            // 
            this.btn_setSourceDir.Location = new System.Drawing.Point(188, 153);
            this.btn_setSourceDir.Name = "btn_setSourceDir";
            this.btn_setSourceDir.Size = new System.Drawing.Size(75, 23);
            this.btn_setSourceDir.TabIndex = 1;
            this.btn_setSourceDir.Text = "Sources";
            this.btn_setSourceDir.UseVisualStyleBackColor = true;
            this.btn_setSourceDir.Click += new System.EventHandler(this.btn_setSourceDir_Click);
            // 
            // btn_setTarget
            // 
            this.btn_setTarget.Location = new System.Drawing.Point(188, 124);
            this.btn_setTarget.Name = "btn_setTarget";
            this.btn_setTarget.Size = new System.Drawing.Size(75, 23);
            this.btn_setTarget.TabIndex = 2;
            this.btn_setTarget.Text = "Target";
            this.btn_setTarget.UseVisualStyleBackColor = true;
            this.btn_setTarget.Click += new System.EventHandler(this.btn_setTarget_Click);
            // 
            // btn_generateImage
            // 
            this.btn_generateImage.Location = new System.Drawing.Point(188, 211);
            this.btn_generateImage.Name = "btn_generateImage";
            this.btn_generateImage.Size = new System.Drawing.Size(75, 23);
            this.btn_generateImage.TabIndex = 3;
            this.btn_generateImage.Text = "Generate";
            this.btn_generateImage.UseVisualStyleBackColor = true;
            this.btn_generateImage.Click += new System.EventHandler(this.btn_generateImage_Click);
            // 
            // lbl_disp_targetPath
            // 
            this.lbl_disp_targetPath.AutoSize = true;
            this.lbl_disp_targetPath.Location = new System.Drawing.Point(12, 129);
            this.lbl_disp_targetPath.Name = "lbl_disp_targetPath";
            this.lbl_disp_targetPath.Size = new System.Drawing.Size(35, 13);
            this.lbl_disp_targetPath.TabIndex = 4;
            this.lbl_disp_targetPath.Text = "label1";
            // 
            // lbl_disp_outputPath
            // 
            this.lbl_disp_outputPath.AutoSize = true;
            this.lbl_disp_outputPath.Location = new System.Drawing.Point(12, 187);
            this.lbl_disp_outputPath.Name = "lbl_disp_outputPath";
            this.lbl_disp_outputPath.Size = new System.Drawing.Size(35, 13);
            this.lbl_disp_outputPath.TabIndex = 6;
            this.lbl_disp_outputPath.Text = "label1";
            // 
            // lbl_disp_status
            // 
            this.lbl_disp_status.AutoSize = true;
            this.lbl_disp_status.Location = new System.Drawing.Point(12, 216);
            this.lbl_disp_status.Name = "lbl_disp_status";
            this.lbl_disp_status.Size = new System.Drawing.Size(46, 13);
            this.lbl_disp_status.TabIndex = 7;
            this.lbl_disp_status.Text = "Waiting.";
            // 
            // dlg_selectOpen
            // 
            this.dlg_selectOpen.FileName = "openFileDialog1";
            // 
            // lbl_disp_sourcePath
            // 
            this.lbl_disp_sourcePath.AutoSize = true;
            this.lbl_disp_sourcePath.Location = new System.Drawing.Point(12, 158);
            this.lbl_disp_sourcePath.Name = "lbl_disp_sourcePath";
            this.lbl_disp_sourcePath.Size = new System.Drawing.Size(35, 13);
            this.lbl_disp_sourcePath.TabIndex = 5;
            this.lbl_disp_sourcePath.Text = "label1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.lbl_disp_status);
            this.Controls.Add(this.lbl_disp_outputPath);
            this.Controls.Add(this.lbl_disp_sourcePath);
            this.Controls.Add(this.lbl_disp_targetPath);
            this.Controls.Add(this.btn_generateImage);
            this.Controls.Add(this.btn_setTarget);
            this.Controls.Add(this.btn_setSourceDir);
            this.Controls.Add(this.btn_setOutput);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_setOutput;
        private System.Windows.Forms.Button btn_setSourceDir;
        private System.Windows.Forms.Button btn_setTarget;
        private System.Windows.Forms.Button btn_generateImage;
        private System.Windows.Forms.Label lbl_disp_targetPath;
        private System.Windows.Forms.Label lbl_disp_outputPath;
        private System.Windows.Forms.Label lbl_disp_status;
        private System.Windows.Forms.FolderBrowserDialog dlg_selectFolder;
        private System.Windows.Forms.OpenFileDialog dlg_selectOpen;
        private System.Windows.Forms.SaveFileDialog dlg_selectSave;
        private System.Windows.Forms.Label lbl_disp_sourcePath;
    }
}

