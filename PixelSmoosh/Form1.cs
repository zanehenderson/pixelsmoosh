﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PixelSmoosh
{
    public partial class Form1 : Form
    {
        private string _targetPath = "";
        private string _sourcePath = "";
        private string _outputPath = "";

        public Form1()
        {
            InitializeComponent();
        }

        private void btn_setTarget_Click(object sender, EventArgs e)
        {
            DialogResult result = dlg_selectOpen.ShowDialog(); // Show the dialog.
            if (result == DialogResult.OK) // Test result.
            {
                _targetPath = dlg_selectOpen.FileName;
                lbl_disp_targetPath.Text = _targetPath;
            }
            // Cancel button was pressed.
            else if (result == DialogResult.Cancel)
            {
                return;
            }
        }

        private void btn_setSourceDir_Click(object sender, EventArgs e)
        {
            DialogResult result = dlg_selectFolder.ShowDialog(); // Show the dialog.
            if (result == DialogResult.OK) // Test result.
            {
                _sourcePath = dlg_selectFolder.SelectedPath;
                lbl_disp_sourcePath.Text = _sourcePath;
            }
            // Cancel button was pressed.
            else if (result == DialogResult.Cancel)
            {
                return;
            }
        }

        private void btn_setOutput_Click(object sender, EventArgs e)
        {
            DialogResult result = dlg_selectSave.ShowDialog(); // Show the dialog.
            if (result == DialogResult.OK) // Test result.
            {
                _outputPath = dlg_selectSave.FileName;
                lbl_disp_outputPath.Text = _outputPath;
            }
            // Cancel button was pressed.
            else if (result == DialogResult.Cancel)
            {
                return;
            }
        }

        private void btn_generateImage_Click(object sender, EventArgs e)
        {
            System.IO.DirectoryInfo sourceDir = new System.IO.DirectoryInfo(_sourcePath);

            System.IO.FileInfo[] files = null;
            files = sourceDir.GetFiles("*.*");
            
            if (files == null)
            {
                return;
            }

            LockBitmap[] sources = new LockBitmap[files.Length-1];
            for (int i = 0; i < sources.Length; i++)
            {
                sources[i] = new LockBitmap((Bitmap)Image.FromFile(files[i].FullName));
                sources[i].LockBits();
            }

            Bitmap bmp = (Bitmap)Image.FromFile(_targetPath);
            LockBitmap result = new LockBitmap(bmp);
            result.LockBits();

            Color tCol;
            Color sCol;
            
            for (int y = 0; y < result.Height; y++)
            {
                for (int x = 0; x < result.Width; x++)
                {
                    int r = 0, g = 0, b = 0, diff = 0, nearest = 256;
                    Color nearestCol = Color.Black;

                    tCol = result.GetPixel(x, y);
                    foreach( LockBitmap source in sources)
                    {
                        sCol = source.GetPixel(x, y);

                        r = Math.Abs(tCol.R - sCol.R);
                        g = Math.Abs(tCol.G - sCol.G);
                        b = Math.Abs(tCol.B - sCol.B);

                        diff = ((r + g + b) / 3);
                        if (diff < nearest)
                        {
                            nearest = diff;
                            nearestCol = sCol;
                        }
                    }
                    result.SetPixel(x, y, nearestCol);
                }
                
            }

            foreach (LockBitmap source in sources)
            {
                source.UnlockBits();
            }

            result.UnlockBits();
            bmp.Save(_outputPath);
        }
    }
}
